var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const RolController = require('../controller/RolController');
const CuentaController = require('../controller/CuentaController');
const PersonaController = require('../controller/PersonaController');
const AutoController = require('../controller/AutoController');
const RepuestoController = require('../controller/RepuestoController');
const MarcaController = require('../controller/MarcaController');
const FacturaController = require('../controller/FacturaController');
const PagoController = require('../controller/PagoController');
const models = require('../models');

var rolController = new RolController();
var cuentaController = new CuentaController();
var personaController = new PersonaController();
var autoController = new AutoController();
var repuestoController = new RepuestoController();
var marcaController = new MarcaController();
var facturaController = new FacturaController();
var pagoController = new PagoController();

var auth = function middleware(req, res, next) {
  console.log("hola");
  const token = req.headers['x-api-token'];
  console.log(req.headers);
  console.log(token);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log(err);
        res.status(401);
        res.send("token no valido");
        res.json({ msg: "Token no valido", code: 401 });
      } else {
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (aux === null) {
          res.status(401);
          res.send("token no valido");
          res.json({ msg: "Token no valido", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.send("no existe el token");
    res.json({ msg: "No existe el token!!", code: 401 });
  }
};
const diskStorage = multer.diskStorage({
  destination: path.join(__dirname, '../public/images'),
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname)
  }
})
const fileUpload = multer({
  storage: diskStorage
}).single('image');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json(
    {
      "version": "1.0",
      "name": "auto"
    }
  );
});
router.post('/cargar/foto', fileUpload, (req, res) => {
  // Acciones para guardar la imagen en la base de datos o realizar otras operaciones con ella
  req.getConnection((err, conn) => {
    if (err) return res.status(500).send('Error del servidor');
    const type = req.file.mimetype;
    const name = req.file.originalname;
    const data = fs.readFileSync(path.join(__dirname, '../public/images', req.file.filename));
    conn.query('INSERT INTO image set ?', [{ type, name, data }], (err, rows) => {
      if (err) return res.status(500).send('Error del servidor');
      res.send('¡Imagen guardada!');
    });
  });
});
router.post('/checkout/guardar', pagoController.guardar);
router.get('/checkout/obtener/:checkoutId', pagoController.obtener);
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

// Roles
router.get('/roles', rolController.listar);

// Personas
router.put('/personas/modificar', personaController.modificar);
router.post('/personas/guardar', personaController.guardar, [
  body('apellidos', 'Ingrese su apellido').exists(), 
  body('nombres', 'Ingrese algun dato').exists()
]);
router.post('/sesion', cuentaController.sesion);
router.get('/personas', auth, personaController.listar);
router.get('/personas/listar', personaController.listar);
router.get('/personas/obtener/:external', auth, personaController.obtener);

// Autos
router.get('/autos',autoController.listar);
router.post('/auto/guardar',autoController.guardar);
router.post('/autos/modificar',auth,autoController.modificar);
router.get('/auto/obtener/:external',auth,autoController.obtener);
router.get('/autos/disponibles',autoController.listarDisponibles);
router.get('/autos/vendidos',auth,autoController.listarVendidos);
router.get("/autos/ingresarReparacion",auth,autoController.listarIngresarReparacion);
router.get('/autos/num',auth,autoController.numAuto);
//router.get('/autos/modelo', autoController.listarModelo);
//router.get('/autos/cantidad', autoController.cantAutos);

// Repuestos
router.post('/repuesto/guardar', repuestoController.guardar);
router.get('/repuestos/listar', repuestoController.listarRepuesto);

// Marcas
router.get('/marcas/listar', marcaController.listar);
router.get('/marcas/cantidad', marcaController.cantMarcas);

// Facturas
router.post('/factura/guardar', facturaController.guardar);
router.get('/facturas/listar', facturaController.listarInformacion);

module.exports = router;
