'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var repuesto = models.repuesto;
var auto = models.auto;
class RepuestoController{
    async listarRepuesto(req, res) {
        var listar = await repuesto.findAll({
            attributes: [
                'external_id',
                'nombre',
                'costoUnitario',
                'marca'
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, info: listar });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
                    var data = {
                        nombre: req.body.nombre,
                        costoUnitario: req.body.costoUnitario,
                        marca: req.body.marca
                    };
                    res.status(200);
                    //let transaction = await models.sequelize.transaction();
                    try {
                        await repuesto.create(data);
                        //await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

           
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
            
    }
}
module.exports = RepuestoController;