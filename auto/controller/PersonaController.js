'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const salRounds = 8;
class PersonaController {
    async listar(req, res) {
        var listar = await persona.findAll({
            attributes: ['apellidos', 'nombres', 'external_id', 'direccion', 'fechaNacimiento', 'identificacion', 'tipo']
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: listar });

    }
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external }, include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'fechaNacimiento', 'tipo']
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var rol_id = req.body.external_rol;
            if (rol_id != undefined) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(salRounds), null);
                    };
                    //data arreglo asociativo= es un direccionario = clave:valor
                    /*{
  "external_rol":"8dd7ce5d-0bd8-11ee-ad10-0c84dc93cded",
  "tipo":"CEDULA",
  "identificacion":"1150887105",
  "nombres":"jordy",
  "apellidos":"torres",
  "direccion":"julio ordoñez",
  "usuario":"jordy",
  "clave":"1234"
}
*/
                    var data = {
                        identificacion: req.body.identificacion,
                        tipo: req.body.tipo,
                        nombres: req.body.nombres,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        fechaNacimiento: req.body.fechaNacimiento,
                        id_rol: rolAux.id,
                        cuenta: {
                            usuario: req.body.usuario,
                            clave: claveHash(req.body.clave)
                        }
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }


            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    async modificar(req, res) {
        var person = await persona.finOne({ where: { external_id: req.body.external } });
        if (persona === null) {
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.apellidos = req.body.apellidos;
            person.nombres = req.body.nombre;
            person.direccion = req.body.direccion;
            person.fechaNacimiento = req.body.direccion;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {
                res.status(400);
            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }
}

module.exports = PersonaController;