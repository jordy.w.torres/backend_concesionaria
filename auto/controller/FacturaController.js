'use strict';
const { body, validationResult, check } = require('express-validator');
const models = require('../models');
const facturaReparacion = models.facturaReparacion;
const persona = models.persona;
const auto = models.auto;
const repuesto = models.repuesto;

class FacturaController {

    // Otros métodos del controlador...
    async listarInformacion(req, res) {
        try {
          const facturas = await facturaReparacion.findAll({
            attributes: ['numerofactura','total','fecha_emision'],
            include: [
              {
                model: persona,
                attributes: ['identificacion'],
              },
              {
                model: auto,
                attributes: ['placa', 'modelo'],
              },
              {
                model: repuesto,
                attributes:['nombre']
              }
            ],
          });
      
          res.json({ msg: "OK", code: 200, info: facturas });
        } catch (error) {
          console.log(error);
          res.status(500).json({ msg: "Ocurrió un error al obtener la información", code: 500 });
        }
      }
      

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            try {
                const obtenerIdPorCedula = async (cedula) => {
                    try {
                        const personaEncontrada = await persona.findOne({
                            where: { identificacion: cedula },
                            attributes: ['id']
                        });

                        if (personaEncontrada) {
                            console.log(personaEncontrada.id + " Encontrado id");
                            return personaEncontrada.id;
                        } else {
                            res.status(404).json({ msg: "No se encontró ninguna persona con la cédula proporcionada", code: 404, error: "Persona no encontrada" });
                        }
                    } catch (error) {
                        console.log(error);
                        res.status(500).json({ msg: "Error al obtener el ID por cédula", code: 500, error: "Error al obtener ID por cédula" });
                    }
                };

                const obtenerIdAutoPorPersona = async (idPersona) => {
                    try {
                        const autoPersona = await auto.findOne({ where: { id_persona: idPersona } });
                        if (autoPersona) {
                            return autoPersona.id;
                        } else {
                            res.status(404).json({ msg: "No se encontró un auto asociado a la persona", code: 404, error: "Auto no encontrado" });
                        }
                    } catch (error) {
                        console.log(error);
                        res.status(500).json({ msg: "Error al obtener el ID del auto por persona", code: 500, error: "Error al obtener ID del auto por persona" });
                    }
                };

                const obtenerCostoUnitarioRepuesto = async (idRepuesto) => {
                    try {
                        const repuestoData = await repuesto.findOne({
                            where: { id: idRepuesto },
                            attributes: ['costoUnitario']
                        });
                        if (repuestoData) {
                            console.log(repuestoData.costoUnitario + " Costo unitario del repuesto");
                            return repuestoData.costoUnitario;
                        } else {
                            res.status(404).json({ msg: "No se encontró el repuesto con el ID proporcionado", code: 404, error: "Repuesto no encontrado" });
                        }
                    } catch (error) {
                        console.log(error);
                        res.status(500).json({ msg: "Error al obtener el costo unitario del repuesto", code: 500, error: "Error al obtener costo unitario del repuesto" });
                    }
                };
                const obtenerIdRepuesto = async (externalId) => {
                    try {
                        const repuestoEncontrado = await repuesto.findOne({
                            where: { external_id: externalId },
                            attributes: ['id']
                        });

                        if (repuestoEncontrado) {
                            return repuestoEncontrado.id;
                        } else {
                            throw new Error("No se encontró ningún repuesto con el external ID proporcionado");
                        }
                    } catch (error) {
                        console.log(error);
                        throw new Error("Error al obtener el ID del repuesto por external ID");
                    }
                };

                const idPersona = await obtenerIdPorCedula(req.body.identificacion);
                const idAuto = await obtenerIdAutoPorPersona(idPersona);
                const rep = req.body.idRepuesto;
                const idRepuesto = await obtenerIdRepuesto(rep)
                const precioRepuesto = await obtenerCostoUnitarioRepuesto(idRepuesto);

                const cantidad = req.body.cantidad;
                const iva = 0.12;
                const subtotal = precioRepuesto * cantidad;
                const total = subtotal + cantidad * iva;


                const data = {
                    fecha_emision: req.body.fechaEmision,
                    direccion: req.body.direccion,
                    subTtotal: subtotal,
                    iva: iva,
                    total: total,
                    cantidad: cantidad,
                    estado: true,
                    id_persona: idPersona,
                    id_auto: idAuto,
                    id_repuesto: idRepuesto
                };

                const factura = await facturaReparacion.create(data);

                res.status(200).json({ msg: "Se ha registrado la factura", code: 200, factura });
            } catch (error) {
                console.log(error);
                res.status(500).json({ msg: "Ocurrió un error al guardar la factura", code: 500, error: "Error al guardar la factura" });
            }
        } else {
            res.status(400).json({ msg: "Datos faltantes", code: 400, errors: errors.array() });
        }
    }
}

module.exports = FacturaController;
