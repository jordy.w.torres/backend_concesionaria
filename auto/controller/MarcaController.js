'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var auto = models.auto;
var persona = models.persona;
var marca = models.marca;
class MarcaController {
    async cantMarcas(req, res) {
        var cant = await marca.count();
        console.log(cant);
        res.json({ msg: "OK!", code: 200, info: cant });
    }
    async listar(req, res) {
        console.log("hola");
        var listar = await marca.findAll({
            attributes: [
                'external_id',
                'nombre'
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, info: listar });
    }
}
module.exports = MarcaController;
