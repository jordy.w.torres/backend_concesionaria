'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const repuesto = sequelize.define('repuesto', {
        nombre: DataTypes.STRING(20),
        marca: DataTypes.STRING(20),
        costoUnitario: {type: DataTypes.DECIMAL(50, 2), defaultValue: 0.00 },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, {freezeTableName: true});

    repuesto.associate = function(models) {
        repuesto.hasMany(models.facturaReparacion, { foreignKey: 'id_repuesto',as:'facturaReparacion' });
    }
    
    return repuesto;    
};
