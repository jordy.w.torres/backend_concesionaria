'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const facturaReparacion= sequelize.define('facturaReparacion', {
        fecha_emision: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
        direccion:{ type: DataTypes.STRING(50), defaultValue: "NO_DATA" },
        numeroFactura: { type: DataTypes.STRING(15),defaultValue:numeroFactura,unique: true},          
        subTtotal:{ type: DataTypes.DECIMAL(50, 2), defaultValue: 0.00 },
        iva:{ type: DataTypes.DECIMAL(50, 2), defaultValue: 0.00 },
        total:{ type: DataTypes.DECIMAL(50, 2), defaultValue: 0.00 },
        cantidad: { type: DataTypes.DECIMAL(50, 2), defaultValue: 0.00 },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
    }, {
        freezeTableName: true
    });

    facturaReparacion.associate = function (models){
        facturaReparacion.belongsTo(models.persona,{foreignKey:'id_persona'})
        facturaReparacion.belongsTo(models.auto, {foreignKey: 'id_auto'});
        facturaReparacion.belongsTo(models.repuesto, {foreignKey: 'id_repuesto'})
    }

    return facturaReparacion;
};

function numeroFactura() {
    const establishmentNumber = '123'; // Número de establecimiento según el RUC
    const invoiceCounter = '001'; // Número de facturero
    const randomNumber = Math.floor(Math.random() * 1000000).toString().padStart(9, '0'); // Número de factura aleatorio de 9 dígitos
    return `${establishmentNumber}${invoiceCounter}${randomNumber}`;
}